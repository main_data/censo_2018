
## install package
rm(list=ls())
require(pacman)
p_load(rio,tidyverse,sf,leaflet,janitor)

## censo data
browseURL("https://microdatos.dane.gov.co//catalog/643/get_microdata")

##=== unzip data ===##

## unzip file
unzip(zipfile="08_ANTLATICO/08_Atlantico_CSV.zip" , exdir="08_ANTLATICO/input" , overwrite=T) 


##=== load data ===##
## mgn
mgn = import("08_ANTLATICO/input/CNPV2018_MGN_A2_08.CSV")
colnames(mgn)
distinct_all(mgn[,c("UA_CLASE","COD_ENCUESTAS","U_VIVIENDA")]) %>% nrow()

## vivienda
viv = import("08_ANTLATICO/input/CNPV2018_1VIV_A2_08.CSV") 
colnames(viv)
distinct_all(viv[,c("COD_ENCUESTAS","U_VIVIENDA")]) %>% nrow()

## hogar
hog = import("08_ANTLATICO/input/CNPV2018_2HOG_A2_08.CSV")
colnames(hog)
distinct_all(hog[,c("UA_CLASE","COD_ENCUESTAS","U_VIVIENDA","H_NROHOG")]) %>% nrow()

## personas
per = import("08_ANTLATICO/input/CNPV2018_5PER_A2_08.CSV") 
colnames(per)
distinct_all(per[,c("UA_CLASE","COD_ENCUESTAS","U_VIVIENDA","P_NROHOG","P_NRO_PER")]) %>% nrow()




##=== join data: a nivel hogar ===##

## vivienda y hogar
viv_hog = left_join(hog,viv,by=c("COD_ENCUESTAS","U_VIVIENDA","UA_CLASE"))
table(is.na(viv_hog$VA1_ESTRATO))

## adicionar el mgn
viv_hog_mgn = left_join(viv_hog,mgn,by=c("UA_CLASE","COD_ENCUESTAS","U_VIVIENDA"))
table(is.na(viv_hog_mgn$VA1_ESTRATO))

## eliminar segunda variable
viv_hog_mgn = viv_hog_mgn %>% 
                select(-c(TIPO_REG.y,U_DPTO.y,U_MPIO.y,U_EDIFICA.y,U_DPTO.x,U_MPIO.x)) %>% 
                rename(TIPO_REG = TIPO_REG.x,
                       U_EDIFICA = U_EDIFICA.x) 

## export data
export(viv_hog_mgn,"08_ANTLATICO/output/vivienda-hogar-mgn.rds")





##=== join data: a nivel persona ===##

## vivienda, hogar, persona y mgn
viv_hog_mgn_per = left_join(per,viv_hog_mgn,
                            by=c("UA_CLASE"="UA_CLASE",
                                 "COD_ENCUESTAS"="COD_ENCUESTAS",
                                 "U_VIVIENDA"="U_VIVIENDA",
                                 "P_NROHOG"="H_NROHOG")) 
table(is.na(viv_hog_mgn_per$VA1_ESTRATO))

viv_hog_mgn_per = viv_hog_mgn_per %>% 
  select(-c(U_DPTO.y,TIPO_REG.y,U_MPIO.y)) %>% 
  rename(TIPO_REG = TIPO_REG.x,
         U_MPIO = U_MPIO.x, 
         U_DPTO = U_DPTO.x) 

## export data
export(viv_hog_mgn_per,"08_ANTLATICO/output/vivienda-hogar-persona-mgn.rds")


##=== delete files ===##
unlink("08_ANTLATICO/input",recursive=T)









